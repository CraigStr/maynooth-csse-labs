# Creates a 2D Array of all possible variations of my student number (80 total)
# Passes each variation through the Colatz Algorithm and counts the number of sequences for each variation
# Prints the longest number of sequences of all of the variations of the student number


max_long = 9223372036854775807  # Number cannot go above Max Long
ss_number = 17718781  # My Studnet number to test against
high_val = 0  # Higtest count of iterations
high_var = 0  # variation that gave the highest number of sequences
ph_num = 0;


def colatz(num):
    while num * 2 <= max_long:
        num *= 2
    global ph_num
    ph_num = num  # Placeholder for original number being tested
    count = 0
    # print(num)
    while num != 1:  # Colatz algorithm
        if num % 2 == 0:
            num /= 2
        else:
            num = (num * 3) + 1
        if num > max_long:  # exit if num is too big (i.e. longer than a long)
            return
        # print(num)
        count += 1
    global high_val, high_var
    if count > high_val:  # update high score if higher than previous
        high_val = count
        high_var = ph_num


list = []  # fill array with split student number
for y in range(len(str(ss_number)) * 10):
    list.append([1, 7, 7, 1, 8, 7, 8, 1])

y = 0

for x in range(len(str(ss_number)) * 10):  # change each index of array to unique variation of student number
    if x % 10 == 0:
        if x > 1:
            y += 1
    list[x][y] = (list[x][y] + x) % 10

for x in list:
    colatz(float(''.join([str(y) for y in x])))

print("\nOriginal Student Number:", "\t\t\t\t", ss_number)
print("Longest sequence was given by number:", "\t", high_var)
print("with Length:", "\t\t\t\t\t\t\t", high_val)
