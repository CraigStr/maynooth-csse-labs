package tk.imcraig.labs210;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class readFromFile {
    public static void main(String[] args) throws IOException {

        List<Integer> ints = Files.lines(Paths.get("/home/craig/Documents/Code/College/CS210/Labs-Java/src/tk/imcraig/labs210/lab06_nums.txt"))
                .map(Integer::parseInt)
                .collect(Collectors.toList());
        int[] a = ints.stream().mapToInt(l -> l).toArray();
        for (int b : a){
            System.out.println(b);
        }
    }
}
