package tk.imcraig.sorting;

import tk.imcraig.sorting.unsorted.readNumstoArray;

public class InsertionSort {
    public static void main(String[] args) {

        readNumstoArray rna = new readNumstoArray();
        InsertionSort is = new InsertionSort();
        for (int i : is.insertionSort(rna.readFile("100"))) {
            System.out.println(i);
        }
    }

    public int[] insertionSort(int[] ar) {
        for (int i = 1; i < ar.length; i++){
            int key = ar[i];
            int j = i -1;
            while (j >= 0 && ar[j] > key){
                ar[j+1] = ar[j];
                j -= 1;
            }
            ar[j+1] = key;
        }
        return ar;
    }

}
