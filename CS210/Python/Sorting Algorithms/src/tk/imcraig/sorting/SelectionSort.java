package tk.imcraig.sorting;

import tk.imcraig.sorting.unsorted.readNumstoArray;

public class SelectionSort {
    public static void main(String[] args) {

        readNumstoArray rna = new readNumstoArray();
        SelectionSort ss = new SelectionSort();
        for (int i : ss.selectionSort(rna.readFile("100"))) {
            System.out.println(i);
        }
    }

    public int[] selectionSort(int[] ar) {

        // One by one move boundary of unsorted subarray
        for (int i = 0; i < ar.length - 1; i++) {
            // Find the minimum element in unsorted array
            int min = i;
            for (int j = i + 1; j < ar.length; j++)
                if (ar[j] < ar[min])
                    min = j;

            // Swap the found minimum element with the first
            // element
            int temp = ar[min];
            ar[min] = ar[i];
            ar[i] = temp;
        }
        return ar;

    }
}
