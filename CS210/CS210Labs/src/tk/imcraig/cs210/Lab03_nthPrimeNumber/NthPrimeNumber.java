package tk.imcraig.cs210.Lab03_nthPrimeNumber;

import java.util.Scanner;

public class NthPrimeNumber {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x = sc.nextInt();
        int count = 1;
        int i = 3;
        boolean isPrime = true;
        while (count != x) {
            for (int j = 2; j < i / 2 + 1; j++) {
                if (i % j == 0) {
                    isPrime = false;
                }
            }
            if (isPrime) {
                count++;
            }
            isPrime = true;
            i++;
        }
        System.out.println(i - 1);
    }
}