package tk.imcraig.cs210.Lab03_nthPrimeNumber;

import java.util.Scanner;

public class DifferenceOfPrimes {
    public static void main(String[] args) {

        DifferenceOfPrimes dp = new DifferenceOfPrimes();

        Scanner sc = new Scanner(System.in);

        int start = sc.nextInt();
        int a = start + 1;
        int b = start - 1;

        while (!dp.isPrime(a)) {
            a++;
        }
        while (!dp.isPrime(b)) {
            b--;
        }

        System.out.println(a);
        System.out.println(b);
        System.out.println(Math.abs(a - b));
    }

    boolean isPrime(int i) {
        for (int j = 2; j < Math.sqrt(i) + 1; j++) {
            if (i % j == 0) {
                return false;
            }
        }
        return true;
    }

}
