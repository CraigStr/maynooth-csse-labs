package tk.imcraig.cs210.Lab02_colatzconjecture;

import java.util.Scanner;

public class colatzConjecture {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        long num = sc.nextLong();
        int count = 0;
        while (num != 1) {
            if (num % 2 == 0) num /= 2;
            else num = (num * 3) + 1;
            System.out.println(num);
            count++;
        }
        System.out.println(count);

    }
}
