package tk.imcraig.cs210.Lab09_linkedlist.testsimple;

public class DoubleLinkedList {
    public static void main(String[] args) {
        DLinkedList ll = new DLinkedList();
    }
}

class DNode {
    DNode next;
    DNode prev;
    int data;

    DNode(int data) {
        this.data = data;
    }
}

class DLinkedList {
    private DNode head;

    void append(int data) {
        if (head == null) {
            head = new DNode(data);
            return;
        }
        DNode current = head;
        while (current.next != null) {
            if (current.next.next == null) {
                current.next.prev = current;
            }
            current = current.next;
        }
        current.next = new DNode(data);
    }

    void prepend(int data) {
        DNode newHead = new DNode(data);
        newHead.next = head;
        head = newHead;
    }

    public void deleteWithValue(int data) {
        if (head == null) return;
        if (head.data == data) {
            head = head.next;
            return;
        }
        DNode current = head;
        while (current.next != null) {
            if (current.next.data == data) {
                current.next = current.next.next;
                return;
            }
            current = current.next;
        }
    }

    void print() {
        DNode current = head;
        while (current != null) {
            System.out.println(current.data);
            current = current.next;
        }
    }

}
