package Lab05;

public interface Prototype extends Cloneable {

    AccessControl clone() throws CloneNotSupportedException;

}
