package Lab08;

public class Mushroom extends PizzaDecorator {

    private final Pizza pizza;

    Mushroom(Pizza pizza) {
        this.pizza = pizza;
    }

    @Override
    public String getDesc() {
        return pizza.getDesc() + ", Mushrooms (1.59)";
    }


    @Override
    public double getPrice() {
        return pizza.getPrice() + 1.59;
    }

}
