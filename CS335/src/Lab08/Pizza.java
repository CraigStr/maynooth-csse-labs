package Lab08;

public interface Pizza {

    String getDesc();

    double getPrice();
}
