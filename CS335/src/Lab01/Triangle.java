package Lab01;

import java.util.Scanner;

/**
 * This class performs calculations on triangle.
 * It includes 2 methods for calculating area and circumference of a triangle
 *
 * @author Craig Stratford
 * @version 1.0
 * @since 13 Feb 2019
 */

public class Triangle {

    /**
     * Using Heron's Formula to calculate area
     *
     * @param a double
     * @param b double
     * @param c double
     * @return area double
     */
    public static double area(double a, double b, double c) {
        double p = perimeter(a, b, c) / 2;
        return Math.sqrt(p * (p - a) * (p - b) * (p - c));
    }

    /**
     * @param a double
     * @param b double
     * @param c double
     * @return perimeter double
     */
    private static double perimeter(double a, double b, double c) {
        return a + b + c;
    }

    /**
     * Main method asks user to input a three side lengths
     * then displays to console the area and perimiter
     * of the corresponding triangle
     *
     * @param args String[]
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double a = sc.nextDouble();
        double b = sc.nextDouble();
        double c = sc.nextDouble();
        System.out.println("Area:\t\t" + area(a, b, c));
        System.out.println("Perimeter:\t" + perimeter(a, b, c));
    }

}
