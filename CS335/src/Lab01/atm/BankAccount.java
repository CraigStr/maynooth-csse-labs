package Lab01.atm;

/**
 * This class performs calculations on a bank account
 * It includes 2 methods for calculating withdrawals and deposits in the account
 *
 * @author Craig Stratford
 * @version 1.0
 * @since 13 Feb 2019
 */
public class BankAccount {

    /**
     * Balance attribute
     */
    private double balance;

    /**
     * constructor with no starting balance
     * New bank account
     */
    public BankAccount() {
        balance = 0;
    }

    /**
     * constructor with starting balance
     *
     * @param initialBalance double
     */
    BankAccount(double initialBalance) {
        balance = initialBalance;
    }

    /**
     * Calculates money in account after deposit
     *
     * @param amount double
     */
    public void deposit(double amount) {
        balance = balance + amount;
    }

    /**
     * Calculates if there are sufficient funds
     * in the account to withdraw from, and if there is,
     * withdraws the amount from the account
     *
     * @param amount double
     * @return successful boolean
     */
    public boolean withdraw(double amount) {
        if (balance >= amount) {
            balance = balance - amount;
            return true;
        } else
            return false;

    }

    /**
     * returns current account balance
     *
     * @return balance double
     */
    double getBalance() {
        return balance;
    }
}

