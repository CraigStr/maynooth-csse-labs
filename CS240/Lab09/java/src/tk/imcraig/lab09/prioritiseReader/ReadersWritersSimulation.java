package tk.imcraig.lab09.prioritiseReader;

public class ReadersWritersSimulation {
    public static void main(String[] args) {

        DataAccessPolicyManager accessManager = new DataAccessPolicyManager();

        Reader reader1 = new Reader(accessManager, 1);
        Reader reader2 = new Reader(accessManager, 2);
        Reader reader3 = new Reader(accessManager, 3);
        Writer writer1 = new Writer(accessManager, 1);

        reader1.start();
        reader2.start();
        reader3.start();
        writer1.start();

    }
}