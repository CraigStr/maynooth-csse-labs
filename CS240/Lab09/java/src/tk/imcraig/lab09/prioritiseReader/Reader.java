package tk.imcraig.lab09.prioritiseReader;

public class Reader extends Thread {
    DataAccessPolicyManager lockManager;
    int id;

    Reader(DataAccessPolicyManager lockManager, int id) {
        this.lockManager = lockManager;
        this.id = id;
    }

    public void run() {
        while (true) {
            lockManager.acquireReadLock();
            System.out.println("Reader " + id + " waiting.");
            try { // Simulate eating activity for a random time
                sleep((int) (Math.random() * 10000));
            } catch (InterruptedException ignored) {
            }
            lockManager.releaseReadLock();
        }

    }
}