package tk.imcraig.lab09.prioritiseWriter;


class DataAccessPolicyManager {
    private int readCount, writeCount;

    private Semaphore mutexReadCount, mutexWriteCount;

    private Semaphore wrt, rdr;

    DataAccessPolicyManager() {
        readCount = 0;
        writeCount = 0;
        mutexReadCount = new Semaphore(1);
        mutexWriteCount = new Semaphore(1);
        wrt = new Semaphore(1);
        rdr = new Semaphore(1);
    }

    void acquireReadLock() {
        rdr.acquire();  // Reader can enter if no writers
        mutexReadCount.acquire();
        readCount = readCount + 1;
        if (readCount == 1) wrt.acquire(); // block writers
        mutexReadCount.release();
        rdr.release();  // allow another in
    }

    void releaseReadLock() {
        mutexReadCount.acquire();
        readCount = readCount - 1;
        if (readCount == 0) wrt.release(); //last reader
        mutexReadCount.release();
    }

    void acquireWriteLock() {
        mutexWriteCount.acquire();
        writeCount = writeCount+1;
        if (writeCount == 1) rdr.acquire(); //Block new readers
        mutexWriteCount.release();
        wrt.acquire();  // wait for existing readers/writer
    }

    void releaseWriteLock() {
        mutexWriteCount.acquire();
        writeCount = writeCount - 1;
        if (writeCount==0) rdr.release();//no more writers
        mutexWriteCount.release();
        wrt.release();
    }
}