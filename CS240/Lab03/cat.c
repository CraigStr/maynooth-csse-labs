#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

int main() {
    char file[10];
    char buffer[10];
    int fd, n;

    printf("File: ");
    scanf("%s", file);

    fd = open(file, O_RDONLY);
    do {
        n = read(fd, buffer, 10); /*Read 10 chars from file*/
        write(1, buffer, n); /*Write chars to text terminal*/
    } while (n == 10); /* keep reading until n < 10 */
    close(fd);
}