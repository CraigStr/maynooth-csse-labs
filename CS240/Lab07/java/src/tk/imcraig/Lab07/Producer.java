package tk.imcraig.Lab07;

import java.util.Date;

public class Producer implements Runnable {
    private Buffer buffer;
    private int id;

    Producer(Buffer buffer, int id) {
        this.buffer = buffer;
        this.id = id;
    }

    public void run() {
        Date message;

        while (true) {
            message = new Date(); // produce an item
            try {
                Thread.sleep(3000); // Sleep for 1000 ms
            } catch (InterruptedException ignored) {
            }
            buffer.insert(message);
            System.out.println("Inserted:" + message.toString() + "\t\t" + id);
        }
    }
}