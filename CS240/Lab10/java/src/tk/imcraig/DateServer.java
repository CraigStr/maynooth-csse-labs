package tk.imcraig;

// This is the Server code, save as DateServer.java

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class DateServer {
    public static void main(String[] args) {
        WorkerThread WT;
        int count = 0;

        try {
            // This creates a listener socket
            ServerSocket sock = new ServerSocket(6013);
            while (true) {
                Socket client = sock.accept();
                count++;
                System.out.println("Finished processing client: " + count);
                new WorkerThread(client, count).start();
            }
        } catch (IOException ioe) {
            System.err.println(ioe);
        }
    }
}

