package tk.imcraig;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

public class WorkerThread extends Thread {

    private Socket client;
    private int count;

    WorkerThread(Socket client, int count) {
        this.client = client;
        this.count = count;
    }

    public void run() {
        try {
            PrintWriter pout = new PrintWriter(client.getOutputStream(), true);
            pout.println(new java.util.Date().toString());

            client.close();
        } catch (IOException ioe) {
            System.err.println(ioe);
        }
    }

}
