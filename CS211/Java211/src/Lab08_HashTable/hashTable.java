package Lab08_HashTable;

import java.util.*;

public class hashTable {
    public static void main(String[] args) {
        Scanner myscanner = new Scanner(System.in);
        int items = myscanner.nextInt();
        myscanner.nextLine();
        String[] contents = new String[items];
        for (int i = 0; i < items; i++) {
            contents[i] = myscanner.nextLine();
        }
        int size = 99991;
        Solution mysolution = new Solution();
        String[] hashtable = mysolution.fill(size, contents);
        HashTable mytable = new HashTable(hashtable);
        Solution mysolution2 = new Solution();   //prevents cheating by using memory
        for (int i = 0; i < items; i++) {
            int rand = (int) (Math.random() * items);
            String temp = contents[i];
            contents[i] = contents[rand];
            contents[rand] = temp;
        }
        int total = 0;
        for (int i = 0; i < items; i++) {
            int slot = mysolution2.find(size, mytable, contents[i]);
            if (!hashtable[slot].equals(contents[i])) {
                System.out.println("error!");
            }
        }
        System.out.println(mytable.gettotal());
    }
}

class HashTable {
    private String[] hashTable;
    private int total = 0;

    HashTable(String[] input) {
        hashTable = input;
    }

    boolean check(int slot, String check) {
        if (hashTable[slot].equals(check)) {
            return true;
        } else {
            total++;
            return false;
        }
    }

    int gettotal() {
        return total;
    }
}

class Solution {
    int find(int size, HashTable mytable, String word) {
        int hi = hashIndex(size, word);
        int js = jumpSize(word);
        while (!mytable.check(hi, word)) {
            hi = (hi + js) % size;
        }
        return hi;
    }

    String[] fill(int size, String[] array) {

        String[] hashtable = new String[size];
        for (String word : array) {
            int hi = hashIndex(size, word);
            int js = jumpSize(word);
            while (hashtable[hi] != null) {
                hi = (hi + js) % size;
            }
            hashtable[hi] = word;
        }
        return hashtable;
    }

    private int hashIndex(int size, String word) {
        int a;
        int b;
        int exp = 29;
        int res = 0;
        //Multiply each char in the string by successive powers of 29 and add them together
        for (int i = 0; i < word.length(); i++) {
            a = word.charAt(i);
            b = (int) Math.pow(exp, i);
            res += (a * b);
        }

        res = res % size;

        return res;
    }

    private int jumpSize(String word) {
        int start = 7;

        for (int i = 0; i < word.length(); i++)
            start = 31 * start + word.charAt(i);

        int max = 7; //Max jump size
        return max - (start % max);
    }

}