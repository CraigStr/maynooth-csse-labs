package Lab03_HuffmanEncoding;

import java.util.PriorityQueue;
import java.util.Scanner;

public class HuffmanEncoding {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String sentence = in.nextLine();

        int[] array = new int[256]; // an array to store all the frequencies

        for (int i = 0; i < sentence.length(); i++) { // go through the sentence
            array[(int) sentence.charAt(i)]++; // increment the appropriate frequencies
        }

        PriorityQueue<Tree> PQ = new PriorityQueue<>(); // make a priority queue to hold the forest of trees

        for (int i = 0; i < array.length; i++) { // go through frequency array
            if (array[i] > 0) { // print out non-zero frequencies - cast to a char

                // create a new Lab03_HuffmanEncoding.Tree
                // set the cumulative frequency of that Lab03_HuffmanEncoding.Tree
                // insert the letter as the root node
                // add the Lab03_HuffmanEncoding.Tree into the PQ

                Tree x = new Tree();
                x.root = new Node();

                x.frequency = array[i];
                x.root.letter = x.root.smallestLetter = (char) i;

                PQ.add(x);


            }
        }

        while (PQ.size() > 1) { // while there are two or more Trees left in the forest

            // IMPLEMENT THE HUFFMAN ALGORITHM

            // when you're making the new combined tree, don't forget to assign a default
            // root node (or else you'll get a null pointer exception)
            // if you like, to check if everything is working so far, try printing out the
            // letter of the roots of the two trees you're combining
            // remember to check the smallest letter to decide which branch to put on the
            // left, and which on the right

            Tree a = PQ.poll();
            Tree b = PQ.poll();

            Tree comp = new Tree();
            comp.root = new Node();

            comp.root.leftChild = a.root;
            assert b != null;
            comp.root.rightChild = b.root;

            comp.frequency = a.frequency + b.frequency;
            //a.root.smallestLetter = (char)Math.min(a.root.smallestLetter, b.root.smallestLetter);

            if ((int) a.root.smallestLetter < (int) b.root.smallestLetter) {

                comp.root.smallestLetter = a.root.smallestLetter;
            } else {
                comp.root.smallestLetter = b.root.smallestLetter;
            }

            PQ.add(comp);

        }

        Tree HuffmanTree = PQ.poll(); // now there's only one tree left - get its codes

        // get all the codes for the letters and print them out
        // call the getCode() method on the HuffmanTree Lab03_HuffmanEncoding.Tree object for each letter in
        // the sentence

        for (int i = 0; i < sentence.length(); i++) {

            assert HuffmanTree != null;
            System.out.print(HuffmanTree.getCode(sentence.charAt(i)));


        }

    }
}

class Node {

    char letter = '@'; // stores letter
    char smallestLetter = '@'; // a nice idea it to track the smallest letter in the tree in a special variable
    // like this

    Node leftChild; // this node's left child
    Node rightChild; // this node's right child

} // end class Lab03_HuffmanEncoding.Node

class Tree implements Comparable<Tree> {
    Node root; // first node of tree
    int frequency = 0;

    Tree() // constructor
    {
        root = null;
    } // no nodes in tree yet

    // the PriorityQueue needs to be able to somehow rank the objects in it
    // thus, the objects in the PriorityQueue must implement an interface called
    // Comparable
    // the interface requires you to write a compareTo() method so here it is:

    public int compareTo(Tree object) {
        if (frequency - object.frequency > 0) { // compare the cumulative frequencies of the tree
            return 1;
        } else if (frequency - object.frequency < 0) {
            return -1; // return 1 or -1 depending on whether these frequencies are bigger or smaller
        } else {
            // Sort based on letters
            char a = this.root.smallestLetter;
            char b = object.root.smallestLetter;

            if (a > b) {
                return 1;
            } else if (a < b) {
                return -1;
            }
            return 0;
        }
    }

    String getCode(char letter) { // we want the code for this letter

        return this._getCode(letter, this.root, ""); // return the path that results
    }

    private String _getCode(char letter, Node current, String path) {
        if (current == null) {
            return null;
        }
        if (current.letter == letter) {
            return path;
        }

        String leftPath = this._getCode(letter, current.leftChild, path + "0");
        if (leftPath != null) {
            return leftPath;
        }

        return this._getCode(letter, current.rightChild, path + "1");
    }

}