package Lab05_Recursion;

import java.util.ArrayList;
import java.util.Scanner;

public class countUniqueChars {
    public static void main(String[] args) {
        Scanner myscanner = new Scanner(System.in);
        String sentence = myscanner.nextLine();
        ArrayList<Character> seen = new ArrayList<Character>();
        System.out.println(countUnique(sentence, seen));
    }

    private static int countUnique(String sentence, ArrayList<Character> seen) {
        if (sentence.length() < 1) return seen.size();
        if (seen.contains(sentence.charAt(0))) return countUnique(sentence.substring(1), seen);
        else {
            seen.add(sentence.charAt(0));
            return countUnique(sentence.substring(1), seen);
        }

    }
}
