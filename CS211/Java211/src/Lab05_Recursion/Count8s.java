package Lab05_Recursion;

import java.util.*;

public class Count8s {
    public static void main(String[] args) {
        Scanner myscanner = new Scanner(System.in);
        long n = myscanner.nextLong();
        System.out.println(count8s(n));
    }

    private static int count8s(long input) {

        if (input == 0) return 0;
        else {
            if (input % 100 == 88) return count8s(input / 10) + 2;
            else if (input % 10 == 8) return count8s(input / 10) + 1;
            else return count8s(input / 10);
        }

    }
}