package Lab04_Quicksort;

import java.util.Scanner;

public class quicksort {


    private void sort(String[] ar, int low, int high) {
        if (low < high) {
            int pi = partitionIt(ar, low, high);

            sort(ar, low, pi - 1);
            sort(ar, pi + 1, high);
        }
    }

    private int partitionIt(String[] ar, int low, int high) {
        String pivot = ar[high];
        int i = (low - 1);
        for (int j = low; j < high; j++) {

            if (compare(ar[j], pivot)) {
                i++;

                String temp = ar[i];
                ar[i] = ar[j];
                ar[j] = temp;
            }
        }

        String temp = ar[i + 1];
        ar[i + 1] = ar[high];
        ar[high] = temp;

        return i + 1;
    }

    private boolean compare(String a, String b) {
        char maxA = 0;
        for (char c : a.toCharArray()) {
            if (c > maxA) maxA = c;
        }
        char maxB = 0;
        for (char c : b.toCharArray()) {
            if (c > maxB) maxB = c;
        }
        if (maxA < maxB) {
            return true;
        } else if (maxA == maxB) {
            int comp = a.compareTo(b);
            return comp <= -1;
        }
        return false;
    }

    private static void printArray(String[] ar) {
        for (String i1 : ar) {
            System.out.println(i1);
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[] ar = new String[sc.nextInt()];
        for (int i = 0; i < ar.length; i++) {
            ar[i] = sc.next();
        }
        sc.close();
        int n = ar.length;

        quicksort ob = new quicksort();
        ob.sort(ar, 0, n - 1);

        printArray(ar);
    }
}