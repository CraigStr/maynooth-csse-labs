package Lab01_Vertcoin_Wallet;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class sha256 {
    private static String sha256(String input) throws NoSuchAlgorithmException {
        byte[] in = hexStringToByteArray(input);
        MessageDigest mDigest = MessageDigest.getInstance("SHA-256");
        byte[] result = mDigest.digest(in);
        StringBuffer sb = new StringBuffer();
        for (byte b : result) {
            sb.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }

    private static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    public static void main(String[] args) {
        String secret = "8011E99F0A4D2E1CE9787B2BB450FDBD900887123861D5ACBA583A21E0E3291F73";
        try {
            System.out.println(sha256(secret));
        } catch (NoSuchAlgorithmException ignored) {
        }
    }
}