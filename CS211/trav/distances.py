from haversine import haversine as hv
import numpy as np
import locations as locs

ar = []
for x in locs.get_locs_latlon():
    a = []
    for y in locs.get_locs_latlon():
        pos1 = x[0:2]
        pos2 = y[0:2]
        h = hv(pos1, pos2)
        a.append(h)
    ar.append(a)
print(*ar, sep="\n")
ar = np.array(ar)
print(ar)
