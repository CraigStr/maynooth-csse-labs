from tsp_solver.greedy import solve_tsp
import csv

with open("distMins.csv", 'r') as csvfile:
    reader = csv.reader(csvfile)
    data = list(reader)
for x in range(len(data)):
    for y in range(len(data)):
        if x <= y:
            data[x] = data[x][0:x:]

for x in range(len(data)):
    for y in range(x):
        data[x][y] = float(data[x][y])

# print(*data, sep="\n")

path = solve_tsp(data, endpoints=(0, 100))
print(path)


def draw(nodes):
    import locations as loc
    import matplotlib.pyplot as plt
    import mplleaflet
    plt.figure(figsize=(50, 50))

    lats = []
    long = []
    locs = loc.get_locs_latlon()

    for x in nodes:
        lats.append(locs[x][0])
        long.append(locs[x][1])

    latitude_list = loc.get_lat_list(None)
    longitude_list = loc.get_long_list(None)

    col = 1 / len(latitude_list)

    for x in range(len(lats)):
        plt.scatter(long[x], lats[x])
    plt.plot([-6.59192] + long, [53.38195] + lats)
    plt.scatter(-6.59192, 53.38195, s=100)

    offset = .00005
    for i in range(len(lats)):
        plt.annotate(i + 1, (long[i] + offset, lats[i] + offset))

    mplleaflet.show()


draw(path)
