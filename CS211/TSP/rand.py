import csv
import locations
from random import randint


def original_time():
    from datetime import datetime, timedelta
    locs = locations.get_locs_latlon()
    time = datetime(100, 1, 1, 8, 0, 0)
    total = 0
    late_total = datetime(1, 1, 1, 0, 0, 0)
    # print("Time", "", "Lateness", "Time Travelled", sep='\t')
    # print("______________________________________________")
    data = list(csv.reader(open('distMins.csv')))
    for row in range(101):
        for column in range(101):
            if row + 1 == column:
                cell = data[row][column]
                total += float(cell)
                str_cell = str(cell).split('.')
                time += timedelta(minutes=int(str_cell[0]),
                                  seconds=int(float('0.' + str_cell[1]) * 60))
                late = time - datetime(100, 1, 1, 8, 0, 0) + timedelta(minutes=locs[column][2])
                late_total += late
    #             print(time.time(), late, "", float(cell), sep='\t')
    # print("______________________________________________")
    # print("Total Lateness:", (late_total.day - 1, "Days"), (late_total.hour, "Hours"), (late_total.minute, "Minutes"),
    #       (late_total.second, "Seconds"), sep='\n')
    # print("Average Lateness:",
    #       timedelta(seconds=(timedelta(days=late_total.day - 1, hours=late_total.hour, minutes=late_total.minute,
    #                                    seconds=late_total.second).total_seconds() / 100)))
    print('\nOriginal Route:', total)


def rand_time():
    from datetime import datetime, timedelta
    min_late = datetime(999, 12, 31, 0, 0, 0)
    min_arr = []
    for x in range(10_000):
        print(x)

        row = 0
        visited = [row]
        locs = locations.get_locs_latlon()
        time = datetime(100, 1, 1, 8, 0, 0)
        total = 0
        late_total = datetime(1, 1, 1, 0, 0, 0)
        data = list(csv.reader(open('distMins.csv')))
        while len(visited) < 101:
            rand = randint(0, 100)
            if rand not in visited:
                visited.append(rand)
        for x in range(len(visited[:len(visited) - 1:])):
            row = visited[x]
            column = visited[x + 1]
            cell = data[row][column]
            total += float(cell)
            str_cell = str(cell).split('.')
            time += timedelta(minutes=int(str_cell[0]),
                              seconds=int(float('0.' + str_cell[1]) * 60))
            late = time - datetime(100, 1, 1, 8, 0, 0) + timedelta(minutes=locs[column][2])
            if late >= timedelta(minutes=30):
                late_total += late
        #     print(row, column, time.time(), late, "", float(cell), sep='\t')
        # print("______________________________________________")
        # print("Total Lateness:", late_total, (late_total.day - 1, "Days"), (late_total.hour, "Hours"),
        #       (late_total.minute, "Minutes"),
        #       (late_total.second, "Seconds"), sep='\n')
        # print("Average Lateness:",
        #       timedelta(seconds=(timedelta(days=late_total.day - 1, hours=late_total.hour, minutes=late_total.minute,
        #                                    seconds=late_total.second).total_seconds() / 100)), '\n')
        if late_total < min_late:
            min_late = late_total
            min_arr = visited
            print("______________________________________________")
            print("Route:", total)
            print("Array:", min_arr)

    print("LateTotal:", min_late)
    print("Array:", min_arr)
    print("New Route:", total)
    return min_arr


def draw(nodes):
    import locations as loc
    import matplotlib.pyplot as plt
    import mplleaflet
    plt.figure(figsize=(50, 50))

    lats = []
    long = []
    locs = loc.get_locs_latlon()

    for x in nodes:
        lats.append(locs[x][0])
        long.append(locs[x][1])

    latitude_list = loc.get_lat_list(None)
    longitude_list = loc.get_long_list(None)

    col = 1 / len(latitude_list)

    for x in range(len(lats)):
        plt.scatter(long[x], lats[x])
    plt.plot([-6.59192] + long, [53.38195] + lats)
    plt.scatter(-6.59192, 53.38195, s=100)

    offset = .00005
    for i in range(len(lats)):
        plt.annotate(i + 1, (long[i] + offset, lats[i] + offset))

    mplleaflet.show()


draw(rand_time())
print()
original_time()
