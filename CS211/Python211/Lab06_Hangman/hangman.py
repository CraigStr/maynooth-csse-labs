import random as rd

hangman = r"""
   ____
  |    |
  |     
  |      
  |     
  |      
 _|_
|   |______
|          |
|__________|
""".split('\n')
hm = []

for x in hangman:  # split hangman ascii art to 2D list
    hm.append(list(x))


def printhm():  # Add limb depending on current lives.
    if lives == 5:
        hm[4][6] = '/'  # l-arm
    elif lives == 4:
        hm[4][8] = '\\'  # l-arm
    elif lives == 3:
        hm[4][7] = '|'  # body-top
        hm[5][7] = '|'  # body-bottom
    elif lives == 2:
        hm[6][6] = '/'  # l-leg
    elif lives == 1:
        hm[6][8] = '\\'  # r-leg
    elif lives == 0:
        hm[3][7] = 'o'  # head
    for x in hm:
        print(*x, sep='')


with open('dictionary.txt') as read:  # split language file to list
    lang = read.read().split('\n')

word = lang[rd.randint(0, len(lang))]  # Select random word from lang list
print(word)
template = '_' * len(word)  # template is (length of word) underscores
print(template)

lives = 6

printhm()

while lives >= 1 and '_' in template:
    inp = input()[0]  # take user input
    if inp not in word:
        lives -= 1  # remove a life if letter not in word#
        printhm()
        print(template, lives)
        continue
    for x in range(len(word)):
        if inp == word[x]:  # if letter at certain position in word, change template to that letter at that position
            template = template[:x:] + \
                       inp + \
                       template[x + 1:]
    printhm()
    print(template, lives)
