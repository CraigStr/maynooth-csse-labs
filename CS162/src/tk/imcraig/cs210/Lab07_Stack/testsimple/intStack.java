package tk.imcraig.cs210.Lab07_Stack.testsimple;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class intStack {

    private List<Integer> stack;

    intStack() {
        stack = new ArrayList<Integer>();
    }

    public int pop() {
        if (!stack.isEmpty()) return stack.remove(0);
        else return -1;
    }

    void push(int i) {
        stack.add(i);
    }

    public int peek() {
        if (!stack.isEmpty()) return stack.get(0);
        else return -1;
    }

    int max() {
        return Collections.max(stack);
    }

    int min() {
        return Collections.min(stack);
    }

    public boolean isEmpty() {
        return stack.isEmpty();
    }
}
