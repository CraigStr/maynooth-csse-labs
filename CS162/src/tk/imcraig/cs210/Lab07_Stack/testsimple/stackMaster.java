package tk.imcraig.cs210.Lab07_Stack.testsimple;

public class stackMaster {

    public static void main(String[] args) {
        intStack stack = new intStack();
        stack.push(4);
        stack.push(10);
        stack.push(-100);
        stack.push(101);
        stack.push(543);
        System.out.println(stack.max());
        System.out.println(stack.min());
    }
}
