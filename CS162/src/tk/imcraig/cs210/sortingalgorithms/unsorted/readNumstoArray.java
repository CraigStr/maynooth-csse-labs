package tk.imcraig.cs210.sortingalgorithms.unsorted;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class readNumstoArray {

    public int[] readFile(String num) {
        BufferedReader abc = null;
        try {
            abc = new BufferedReader(new FileReader("/home/craig/Documents/Code/College/CS210/CS210Labs/src/tk/imcraig/cs210/sortingalgorithms/unsorted/Unsorted" + num + ".txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        List<String> data = new ArrayList<String>();
        String s;
        try {
            assert abc != null;
            while ((s = abc.readLine()) != null) {
                data.add(s);
//                System.out.println(s);
            }
            abc.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

// If you want to convert to a String[]
        int[] dataAr = new int[data.size()];
        for (int i = 0; i < data.size(); i++) {
            dataAr[i] = Integer.parseInt(data.get(i));
        }
        return dataAr;

    }

}
