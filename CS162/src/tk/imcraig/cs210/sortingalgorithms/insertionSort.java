package tk.imcraig.cs210.sortingalgorithms;

import tk.imcraig.cs210.sortingalgorithms.unsorted.readNumstoArray;

public class insertionSort {
    public static void main(String[] args) {

        readNumstoArray rna = new readNumstoArray();
        insertionSort is = new insertionSort();
        for (int i : is.insSort(rna.readFile("100"))) {
            System.out.println(i);
        }
    }

    private int[] insSort(int[] ar) {
        for (int i = 1; i < ar.length; i++) {
            int key = ar[i];
            int j = i - 1;
            while (j >= 0 && ar[j] > key) {
                ar[j + 1] = ar[j];
                j -= 1;
            }
            ar[j + 1] = key;
        }
        return ar;
    }

}
