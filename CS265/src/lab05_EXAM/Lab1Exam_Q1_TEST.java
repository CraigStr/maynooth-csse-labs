package lab05_EXAM;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Lab1Exam_Q1_TEST {
//	/********************/
//	/*	QUESTION 1(a)	*/
//	/********************/
	private static Object[][] testData1a = new Object[][] {
		//test,	speed,	expected
		{"T1a.1ep", -50, 	"INVALID"},
		{"T1a.1ep", 40, 	"THANK YOU"},
		{"T1a.1ep", 70, 	"SLOW DOWN"},
		{"T1a.1ep", 100, 	"TOO FAST"}
	};

	@DataProvider(name = "Q1a")
	public Object[][] getTestData1a(){
		return testData1a;
	}

	@Test(dataProvider = "Q1a")
	public void test_GetSpeed1a(String id, int speed, String expected) {
		assertEquals(LabExam1_Q1.speedMonitor(speed), expected);
	}
	
	
	
	/********************/
	/*	QUESTION 1(b)	*/
	/********************/
	private static Object[][] testData1b = new Object[][] {
		//test,	speed,	expected
		{"T1b.1bv", Integer.MIN_VALUE, 	"INVALID"},
		{"T1b.2bv", -1, 				"INVALID"},
		{"T1b.3bv", 0, 					"THANK YOU"},
		{"T1b.4bv", 60, 				"THANK YOU"},
		{"T1b.5bv", 61, 				"SLOW DOWN"},
		{"T1b.6bv", 80, 				"SLOW DOWN"},
		{"T1b.7bv", 81, 				"TOO FAST"},
		{"T1b.8bv", Integer.MAX_VALUE, 	"TOO FAST"}
	};
	
	@DataProvider(name = "Q1b")
	public Object[][] getTestData1b(){
		return testData1b;
	}

	@Test(dataProvider = "Q1b")
	public void test_GetSpeed1b(String id, int speed, String expected) {
		assertEquals(LabExam1_Q1.speedMonitor(speed), expected);
	}
	
	
	
	
	
	/********************/
	/*	QUESTION 1(c)	*/
	/********************/
	private static Object[][] testData1c = new Object[][] {
		//test,	speed,	expected
		{"T1c.1ct", -10, 	"INVALID"},
		{"T1c.2ct", 30, 	"THANK YOU"},
		{"T1c.3ct", 70, 	"SLOW DOWN"},
		{"T1c.4ct", 120, 	"TOO FAST"}
	};
	
	@DataProvider(name = "Q1c")
	public Object[][] getTestData1c(){
		return testData1c;
	}

	@Test(dataProvider = "Q1c")
	public void test_GetSpeed1c(String id, int speed, String expected) {
		assertEquals(LabExam1_Q1.speedMonitor(speed), expected);
	}
	
}
