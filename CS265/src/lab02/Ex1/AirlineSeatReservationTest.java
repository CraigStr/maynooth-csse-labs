package lab02.Ex1;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

public class AirlineSeatReservationTest {
	
	@Test
	public void Ex1_Test1() {
		boolean expected = false;
		assertEquals(expected, AirlineSeatReservations.seatsAvailable(50, 75));
	}
	@Test
	public void Ex1_Test2() {
		boolean expected = true;
		assertEquals(expected, AirlineSeatReservations.seatsAvailable(50, 25));
	}
	@Test
	public void Ex1_Test3() {
		boolean expected = false;
		assertEquals(expected, AirlineSeatReservations.seatsAvailable(-100, 25));
	}
	@Test
	public void Ex1_Test4() {
		boolean expected = false;
		assertEquals(expected, AirlineSeatReservations.seatsAvailable(200, 25));
	}
	@Test
	public void Ex1_Test5() {
		boolean expected = false;
		assertEquals(expected, AirlineSeatReservations.seatsAvailable(50, -100));
	}
	@Test
	public void Ex1_Test6() {
		boolean expected = false;
		assertEquals(expected, AirlineSeatReservations.seatsAvailable(50, 200));
	}
	
}
