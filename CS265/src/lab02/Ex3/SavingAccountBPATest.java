package lab02.Ex3;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

public class SavingAccountBPATest {

	double delta = 0.00000001;
	
	@Test
	public void Ex2_Test1() {
		double expectedValue = 0.00;
		assertEquals(expectedValue, SavingAccountBPA.discountRate(Integer.MIN_VALUE), delta);
	}
	@Test
	public void Ex2_Test2() {
		double expectedValue = 0.00;
		assertEquals(expectedValue, SavingAccountBPA.discountRate(0), delta);
	}
	@Test
	public void Ex2_Test3() {
		double expectedValue = 0.03;
		assertEquals(expectedValue, SavingAccountBPA.discountRate(1), delta);
	}
	@Test
	public void Ex2_Test4() {
		double expectedValue = 0.03;
		assertEquals(expectedValue, SavingAccountBPA.discountRate(100), delta);
	}
	@Test
	public void Ex2_Test5() {
		double expectedValue = 0.05;
		assertEquals(expectedValue, SavingAccountBPA.discountRate(101), delta);
	}
	@Test
	public void Ex2_Test6() {
		double expectedValue = 0.05;
		assertEquals(expectedValue, SavingAccountBPA.discountRate(1000), delta);
	}
	@Test
	public void Ex2_Test7() {
		double expectedValue = 0.07;
		assertEquals(expectedValue, SavingAccountBPA.discountRate(1001), delta);
	}
	@Test
	public void Ex2_Test8() {
		double expectedValue = 0.07;
		assertEquals(expectedValue, SavingAccountBPA.discountRate(Integer.MAX_VALUE), delta);
	}
}
