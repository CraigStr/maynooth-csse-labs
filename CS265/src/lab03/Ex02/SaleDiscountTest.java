package lab03.Ex02;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

public class SaleDiscountTest {
	@Test
	public void Ex2Test01() {
		SaleDiscount ins = new SaleDiscount();
		double expected = 0.02;
		assertEquals(expected, ins.saleRate(25, true, false));
	}

	@Test
	public void Ex2Test02() {
		SaleDiscount ins = new SaleDiscount();
		double expected = 0.02;
		assertEquals(expected, ins.saleRate(25, false, true));
	}

	@Test
	public void Ex2Test03() {
		SaleDiscount ins = new SaleDiscount();
		double expected = 0;
		assertEquals(expected, ins.saleRate(25, false, false));
	}

	@Test
	public void Ex2Test04() {
		SaleDiscount ins = new SaleDiscount();
		double expected = 0.04;
		assertEquals(expected, ins.saleRate(25, true, true));
	}

	@Test
	public void Ex2Test05() {
		SaleDiscount ins = new SaleDiscount();
		double expected = 0.06;
		assertEquals(expected, ins.saleRate(75, true, true));
	}

	@Test
	public void Ex2Test06() {
		SaleDiscount ins = new SaleDiscount();
		double expected = 0.06;
		assertEquals(expected, ins.saleRate(75, true, false));
	}

	@Test
	public void Ex2Test07() {
		SaleDiscount ins = new SaleDiscount();
		double expected = 0.06;
		assertEquals(expected, ins.saleRate(75, false, true));
	}

	@Test
	public void Ex2Test08() {
		SaleDiscount ins = new SaleDiscount();
		double expected = 0.04;
		assertEquals(expected, ins.saleRate(75, false, false));
	}

}
