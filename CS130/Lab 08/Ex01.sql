SELECT *
FROM	lab8_stores as st,
		lab8_sells as se,
		lab8_products as pr
WHERE 	st.storeid = se.storeid
	AND se.productid = pr.productid
	AND st.storestate = 'Florida'