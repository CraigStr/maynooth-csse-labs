-- SELECT * FROM cs130_lab7_property
DELETE FROM cs130_lab7_property
	WHERE agent = 'Property Kings'
		AND housetype = 'Semi-detached'
		AND energyrating IN ('C', 'D');