-- SELECT * FROM lab7_enrolledon
SELECT *
FROM lab7_students AS S,
	lab7_enrolledon AS E,
	lab7_modules AS Mo
	WHERE Mo.modulecredits between 10 and 15
		AND S.studentcourse ~ '^[(BSc)(BA)]'
		AND S.studentid = E.studentid
		AND Mo.moduleid = E.moduleid