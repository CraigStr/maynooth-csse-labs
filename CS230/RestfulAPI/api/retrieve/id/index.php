<?php

$id = 0;
if (isset($_GET['id']))
    $id = $_GET['id'];

$file = "../../../data.json";

$strJsonFileContents = file_get_contents($file);
$array = json_decode($strJsonFileContents, true);

$found = false;

foreach ($array as $key => $item) {
    if ($item['id'] == $id) {
        echo "{\"" . $key . "\":" . json_encode($array[$key]) . "}";
        $found = true;
    }
}

if (!$found) {
    http_response_code(404);
    return;
}

http_response_code(200);
return;