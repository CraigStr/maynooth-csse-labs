<?php

$id = 0;
$name = "";
if (isset($_POST['id']))
    $id = $_POST['id'];
if (isset($_POST['name']))
    $name = $_POST['name'];

$file = "../../../data.json";

$strJsonFileContents = file_get_contents($file);
$array = json_decode($strJsonFileContents, true);

$found = false;

$oldname = "";

foreach ($array as $key => $item) {
    if ($name == $key) {
        http_response_code(409);
        return;
    }
}

foreach ($array as $key => $item) {
    if ($item['id'] == $id) {
        $oldname = $key;
    }
}

$array = replace_key($array, $oldname, $name);

if (!$found) {
    http_response_code(404);
    return;
}

$json = json_encode($array);
file_put_contents($file, $json);

http_response_code(204);
return;

function replace_key($arr, $oldkey, $newkey)
{
    global $found;
    if (array_key_exists($oldkey, $arr)) {
        $found = true;
        $keys = array_keys($arr);
        $keys[array_search($oldkey, $keys)] = $newkey;
        return array_combine($keys, $arr);
    }
    return $arr;
}