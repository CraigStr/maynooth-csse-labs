$(document).ready(function () {
    $('.tabs').tabs();
    $('.modal').modal();
    $('select').formSelect();
});

function toggleModal() {
    if ($('#modal1').isOpen) $('#modal1').modal('open');
    else $('#modal1').modal('open');
}

function syntaxHighlight(json) {
    if (typeof json != 'string') {
        json = JSON.stringify(json, null, '\t');
    }
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        let cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}

$('form').submit(function (event) {
    event.preventDefault();
    $.ajax({
        url: $(this).attr('action'),
        type: $(this).attr('method'),
        data: $(this).serializeJSON(),

        success: function (data, status, xhr) {
            try {
                const json = JSON.parse(data.replace("}{", "},{"));
                $('.modal-content').html('<pre>' + xhr.status + ": " + xhr.statusText + '</pre> <br>' +
                    '<pre>' + syntaxHighlight(json) + '</pre>');
                toggleModal();
            } catch (e) {
                $('.modal-content').html('<pre>' + xhr.status + ": " + xhr.statusText + '</pre>');
                toggleModal();
            }
        },
        error: function (data, status, xhr) {
            $('.modal-content').html('<pre>' + data.status + ": " + data.statusText + '</pre>');
            toggleModal();
        }
    });
});
