/*
1.  Use JavaScript, to highlight a complete table data row when a student's name is selected.
    Clicking again deselects the row. Note you should not be able to select and highlight the title row! (1 point)
        DONE

2.  Use JavaScript, to highlight a complete table Assignment Grade column when a column's title is selected.
    Clicking again deselects the column. Note you should not be able to select and highlight the Final (average) grade column! (1 point)
        DONE

3.  Use JavaScript, and any method of your choosing, to delete a data row selected by a user. (1 point)
        DONE

4.  Use JavaScript, and any method of your choosing, to delete an assignment column selected by a user.
    The function should ensure that the Final Grade column totals are updated following this deletion. (1 point)
        DONE

5.  Use JavaScript, to undelete the last deleted row or column You don't have to use cookies for this; it's acceptable to use a variable if you wish.
    You should consider that once the row/column is undeleted, the button "loses focus", i.e. cannot be clicked again until another row/column is deleted.
    This is really only for students that want a significant challenge. Don't waste time on this unless you've done everything else. (1 point)
*/

addListeners();

function calculateGrades() {
    const table = document.getElementById('grades');
    for (let i = 1; i < table.rows.length; i++) {
        let avg = 0;
        for (let j = 2; j < table.rows[i].cells.length - 1; j++) {
            if (/^\d+.?\d*$/.test(table.rows[i].cells[j].innerHTML)) {
                avg += parseFloat(table.rows[i].cells[j].innerHTML);
            }
        }
        table.rows[i].cells[table.rows[i].cells.length - 1].innerHTML = Math.round(avg / (table.rows[i].cells.length - 3)).toString() + '%';
    }
    Array.prototype.forEach.call(document.querySelectorAll('.finalGrade'), function (item) {
        if (Number(item.innerHTML.replace('%', '')) < 40) {
            item.style.backgroundColor = 'red';
            item.style.color = 'white';
        }
        if (Number(item.innerHTML.replace('%', '')) >= 40) {
            item.style.backgroundColor = 'white';
            item.style.color = 'black';
        }

    });
    Array.prototype.forEach.call(document.querySelectorAll('.assignment'), function (item) {
        if (Number(item.innerHTML === '-')) {
            item.style.backgroundColor = 'yellow';
        } else {
            item.style.backgroundColor = 'white';
        }
    });
}


function newRow() {
    const table = document.getElementById('grades').tBodies[0];
    // const newRow = table.rows[1].cloneNode(true);
    const newRow = document.createElement('tr');
    // const newCell = document.createElement('td');
    for (let i = 0; i < table.rows[0].cells.length; i++) {
        newRow.insertCell(newRow.length);
        newRow.cells[i].innerHTML = '-';
        if (i < table.rows[0].cells.length - 1) {
            // if (i < 1) {
            //     newRow.cells[i].setAttribute('class', 'hlr');
            // }
            if (i < 2) {
                newRow.cells[i].setAttribute('class', 'student_info');
                newRow.cells[i].setAttribute('contentEditable', 'true');
            } else {
                newRow.cells[i].setAttribute('class', 'assignment');
                newRow.cells[i].setAttribute('contentEditable', 'true');
            }
        } else {
            newRow.cells[i].setAttribute('class', 'finalGrade');
        }
    }
    table.appendChild(newRow);
    addListeners()
}

function newColumn() {
    const tbl = document.getElementById('grades');
    for (let i = 1; i < tbl.rows.length; i++) {
        tbl.rows[i].insertCell(tbl.rows[i].cells.length - 1);
        tbl.rows[i].cells[tbl.rows[i].cells.length - 2].innerHTML = '-';
        tbl.rows[i].cells[tbl.rows[i].cells.length - 2].setAttribute('contentEditable', 'true');
        tbl.rows[i].cells[tbl.rows[i].cells.length - 2].setAttribute('class', 'assignment');
    }

    const node = document.createElement('th');
    const head = document.getElementById('tableHead');
    node.innerText = 'Assignment' + (tbl.rows[0].cells.length - 2).toString();
    head.insertBefore(node, document.getElementById('headFinal'));
    addListeners();
}

function deleteRow() {
    const delrow = document.getElementById('delrow').value;
    const table = document.getElementById('grades');
    if (delrow >= 1 && table.rows.length >= 2 && table.rows.length - 1 >= delrow) {
        deletedType = "row";
        deletedIndex = parseInt(delrow) + 1;

        for (let i = 0; i < table.rows[0].cells.length; i++) {
            lastDeleted.push(table.rows[delrow].cells[i].innerHTML.toString());
        }

        table.deleteRow(document.getElementById('delrow').value);
    }
}

function deleteColumn() {
    const delcol = parseInt(document.getElementById('delcol').value) + 1;
    const table = document.getElementById('grades');
    if (delcol < table.rows[0].cells.length - 1 && delcol - 1 >= 1) {
        lastDeleted = [];
        deletedType = "col";
        deletedIndex = delcol + 1;
        for (let i = 0; i < table.rows.length; i++) {
            lastDeleted.push(table.rows[i].cells[parseInt(delcol)].innerHTML);
            table.rows[i].deleteCell(parseInt(delcol));
        }
    }
    resetHeader();
    calculateGrades();
}

function resetHeader() {
    const row = document.getElementById('grades').rows[0];
    for (let i = 2; i < row.cells.length - 1; i++) {
        row.cells[i].innerHTML = 'Assignment ' + (i - 1).toString();
    }
}

// Assign EventListener to each <td> with class assignment
function addListeners() {
    calculateGrades();
    finishedAssignments();
    let assignment = document.querySelectorAll('.assignment');
    const table = document.getElementById('grades');
    for (let i = 0; i < assignment.length; i++) {
        assignment[i].addEventListener('focusout', validateCell);
        assignment[i].addEventListener('keyup', validateCell);
        assignment[i].addEventListener('keyup', calculateGrades);
        assignment[i].addEventListener('keyup', finishedAssignments);
        assignment[i].addEventListener('keyup', finishedAssignments);
    }
    for (let i = 1; i < table.rows.length; i++) {
        table.rows[i].cells[0].addEventListener('click', highlightRow);
    }
    for (let i = 2; i < table.rows[0].cells.length - 1; i++) {
        table.rows[0].cells[i].addEventListener('click', highlightCol);
    }
}

// Automatically removes validateCell when typing into a grade cell
function validateCell() {
    if (!this.innerHTML.match(/^\d*(\.\d*)?$/)) {
        this.innerHTML = this.innerHTML.replace(/[^\d.]/g, '');
    }
    if (parseFloat(this.innerHTML) > 100) {
        this.innerHTML = this.innerHTML.substring(0, 2);
    }
    if (this.innerHTML.trim() === '') {
        this.innerHTML = '-';
    }
    const sel = window.getSelection();
    sel.collapse(this.firstChild, this.innerHTML.length);
}

function finishedAssignments() {
    const table = document.getElementById('grades');
    let count = 0;
    for (let i = 1; i < table.rows.length; i++) {
        for (let j = 2; j < table.rows[i].cells.length - 1; j++) {
            if (table.rows[i].cells[j].innerHTML === '-') {
                count++;
            }
        }
    }
    let AssCount = document.getElementById('AssignmentCount');
    AssCount.innerHTML = AssCount.innerHTML.replace(/\d+/, count.toString());
}

function toCSV() {
    const csv = [], rows = document.querySelectorAll('table tr');
    for (let i = 0; i < rows.length; i++) {
        const row = [], cols = rows[i].querySelectorAll('td, th');

        for (let j = 0; j < cols.length; j++) {
            row.push(cols[j].innerText.replace('\n', ''));
        }
        let delimiter = document.getElementById('delimiter').value;
        if (delimiter.match(/^\s*$/)) {
            delimiter = ',';
        }
        csv.push(row.join(delimiter));
    }
    document.getElementById('csv').value = csv.join('\n');
}

function tableToCookie() {
    toCSV();
    const CSVText = document.getElementById('csv').value;
    document.cookie = 'table=' + encodeURIComponent(CSVText) + ';expires=' + (1000 * 60).toString() + ';path=/';
}

function cookieToTable() {
    const table = document.getElementById('grades');
    let array = decodeURIComponent(document.cookie).substring(decodeURIComponent(document.cookie).indexOf('Name')).split('\n');
    let splitAr = [];
    for (let i = 0; i < array.length; i++) {
        splitAr[i] = array[i].split(',')
    }
    if (splitAr[0].length === 1) return;
    let content = '';
    let count = 0;
    splitAr.forEach(function (row) {
        content += '<tr>';
        row.forEach(function (cell) {
            if (count === 0) content += '<th>' + cell + '</th>';
            else content += '<td>' + cell + '</td>';
        });
        count++;
        content += '</tr>';
    });
    table.children[0].innerHTML = content;
    tableAddClass();
    addListeners();
    calculateGrades();
}

function tableAddClass() {
    const table = document.getElementById('grades');
    table.children[0].children[0].setAttribute('id', 'tableHead');
    for (let i = 0; i < table.rows.length; i++) {
        for (let j = 0; j < table.rows[0].cells.length; j++) {
            if (i === 0 && j === table.rows[0].cells.length - 1) {
                table.rows[0].cells[table.rows[0].cells.length - 1].setAttribute('id', 'headFinal');
            } else if (i !== 0) {
                if (j === table.rows[i].cells.length - 1) {
                    table.rows[i].cells[j].setAttribute('class', 'finalGrade');
                    break;
                }
                if (j < 2) table.rows[i].cells[j].setAttribute('class', 'student_info');
                else table.rows[i].cells[j].setAttribute('class', 'assignment');

                table.rows[i].cells[j].setAttribute('contentEditable', 'true');
            }
        }
    }
}


function highlightRow() {
    let el = this.parentElement;
    for (let i = 0; i < el.children.length; i++) {
        el.children[i].classList.toggle('HLRow');
    }
}

function highlightCol() {
    const table = document.getElementById('grades');
    for (let i = 0; i < table.rows.length; i++) {
        table.rows[i].cells[this.cellIndex].classList.toggle('HLCol');
    }
}

let lastDeleted = [];
let deletedType = null;
let deletedIndex = null;

function unDelete() {
    if (lastDeleted === null) return;

    const table = document.getElementById('grades');
    if (deletedType === 'col') {
        for (let i = 1; i < table.rows.length; i++) {
            let node = table.rows[i].insertBefore(document.createElement('td'), table.rows[i].cells[deletedIndex - 1]);
            node.innerHTML = lastDeleted[i];
            node.setAttribute('contentEditable', 'true');
            node.setAttribute('class', 'assignment');
        }
        const node = document.createElement('th');
        const head = document.getElementById('tableHead');
        node.innerText = lastDeleted[0];
        head.insertBefore(node, table.rows[0].cells[deletedIndex - 1]);
    } else if (deletedType === 'row') {
        const newRow = document.createElement('tr');
        for (let i = 0; i < table.rows[0].cells.length; i++) {
            newRow.insertCell(newRow.length);
            newRow.cells[i].innerHTML = lastDeleted[i];
            if (i < table.rows[0].cells.length - 1) {
                if (i < 2) {
                    newRow.cells[i].setAttribute('class', 'student_info');
                    newRow.cells[i].setAttribute('contentEditable', 'true');
                } else {
                    newRow.cells[i].setAttribute('class', 'assignment');
                    newRow.cells[i].setAttribute('contentEditable', 'true');
                }
            } else {
                newRow.cells[i].setAttribute('class', 'finalGrade');
            }
        }
        table.children[0].insertBefore(newRow, table.rows[deletedIndex - 1]);
    }
    resetHeader();
    addListeners();
    lastDeleted = [];
    deletedIndex = null;
    deletedType = null;

}